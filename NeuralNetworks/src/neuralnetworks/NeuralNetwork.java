/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package neuralnetworks;

import java.util.Random;

/**
 *
 * @author Peter
 * @version 1.0
 * Student Number : 5253133
 */
public class NeuralNetwork {
    private static double LEARNING_RATE;
    private static double MOMENTUM;
    
    private static Random RAND = new Random(12345);
    private double[][] examples;
    
    private int[] layers = new int[] {4,4,1};
    
    // 4 input nodes, 4 hidden layer nodes, 1 output node
    // Value of 1 indicates a connection to be randomly replaces with (-0.5 to 0.5)
    private static double[][] weights = new double[][] {
        { 0, 0, 0, 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 1 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 1 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 1 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 1 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0 } // TODO: remove this entry for output node?
    };
    
    private double[] error = new double[9];
    
    public NeuralNetwork (double learning_rate, double momentum, double[][] examples)
    {
        this.LEARNING_RATE = learning_rate;
        this.MOMENTUM = momentum;
        this.examples = examples;
        InitializeWeights();
        
    }
    
    private void InitializeWeights()
    {
        for (int i = 0; i < weights.length; i++) {
            for (int j = 0; j < weights[i].length; j++) {
                if (weights[i][j] == 1) {
                    weights[i][j] = (RAND.nextDouble() * 2 - 1) * 0.5; // Random number between -0.5 and 0.5
                    //System.out.print(weights[i][j] + " ");
                }
            }
            //System.out.println();
        }
    }
    
    /**
     * Calculate the final value
     * @param index the example index to test
     * @return the final value
     */
    private double[] RunTest(int index)
    {
        //for each layer, the inputs and outputs
        double[] inputs = examples[index];
        double[] outputs = null;
        //the matrix offsets (i.e. the index of the inputs and outputs in the weights matrix)
        int outputOffset = inputs.length;
        int inputOffset = 0;
        
        //for each layer
        for(int i = 1; i < layers.length; i++)
        {
            //initialize the outputs to the size of the current layer
            outputs = new double[layers[i]];
            //for each node in the layer
            for(int j = 0; j < layers[i]; j++)
            {
                //summate the weighted inputs
                for(int k = 0; k < layers[i-1]; k++)
                {
                    outputs[j] += (weights[inputOffset+k][outputOffset+j] * inputs[k]);
                }
                
                //sigmoid that value
                outputs[j] = Sigmoid(outputs[j]);
            }
            //move the offset and change the outputs to the next layers inputs
            inputs = outputs;
            inputOffset = outputOffset;
            outputOffset += layers[i];
        }
        
        return outputs;        
    }
    
    /**
     * Backtrack to calculate the error
     * @param output the actual output
     * @param expected the expected result
     */
    private void UpdateError(double output, double expected)
    {
        //calculate the matrix offsets;
        int inputOffset = calculateOffset(layers.length-1);
        int outputOffset = calculateOffset(layers.length);
        int nodeOffset = error.length - layers[layers.length-1];
        
        //calculate actual error
        double actualError = expected - output;
        
        //Iterate in reverse through the neurons
        for(int i = layers.length; i > 0; i--)
        {
            for(int j = 0; j < layers[i]; j++)
            {
                //if it's after the final layer, just enter the error
                if(i == layers.length)
                {
                    error[outputOffset+j] = actualError;
                }
                else
                {
                    //for all other layers, the error of the prevous layer is the 
                    //sum of all the current layers error times the weight
                    for(int k = 0; k < layers[i-1]; k++)
                    {
                        error[inputOffset+k] += weights[inputOffset+k][outputOffset+j]*error[outputOffset+j];
                    }
                }
            }
            
        }
    }
    
    /**
     * Calculate the offset to get to the specified layer
     * @param layer the layer (start at 1)
     * @return the offset
     */
    private int calculateOffset(int layer)
    {
        int count = 0;
        for(int i = 0; i < layer; i++)
            count+= layers[i];
        return count;
    }
    
    
    /**
     * Calculate the Sigmoid value
     * @param t the sum value
     * @return the sigmoid value
     */
    private double Sigmoid(double t)
    {
        return (1/(1 + Math.pow(Math.E, -t)));
    }
    
    /**
     * Calculate the derivative of the sigmoid function
     * @param t the sum value
     * @return the sigmoid derivative value
     */
    private double SigmoidDerivative(double t)
    {
        return Sigmoid(t)*(1 - Sigmoid(t));
    }

}
