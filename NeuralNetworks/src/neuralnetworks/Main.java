/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package neuralnetworks;

import java.util.Random;
 
public class Main {
 
    private static double LEARNING_RATE = 0.1;
    private static double MOMENTUM = 0.5;  
     
    // 4 input bits followed by the expected parity bit
    private static double[][] examples = new double[][] {
        { 0, 0, 0, 0, 0 },
        { 0, 0, 0, 1, 1 },
        { 0, 0, 1, 0, 1 },
        { 0, 0, 1, 1, 0 },
        { 0, 1, 0, 0, 1 },
        { 0, 1, 0, 1, 0 },
        { 0, 1, 1, 0, 0 },
        { 0, 1, 1, 1, 1 },
        { 1, 0, 0, 0, 1 },
        { 1, 0, 0, 1, 0 },
        { 1, 0, 1, 0, 0 },
        { 1, 0, 1, 1, 1 },
        { 1, 1, 0, 0, 0 },
        { 1, 1, 0, 1, 1 },
        { 1, 1, 1, 0, 1 },
        { 1, 1, 1, 1, 0 }
    };
    
    
     
    public static void main(String[] args) {
        
        NeuralNetwork n = new NeuralNetwork(LEARNING_RATE, MOMENTUM, examples);
         
        boolean allClassifiedCorrectly = true;
         
        while (allClassifiedCorrectly) {
            for (int i = 0; i < examples.length; i++) { // Loop through each example in the training set
                for (int j = 0; j < 4; j++) { // Loop through each hidden layer node
                     
                }
            }
        }
 
    }
 
}